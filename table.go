package pdfreport

import (
	"errors"
	"fmt"
	"math"

	"github.com/jung-kurt/gofpdf"
	// "pdfreport/utils"
	"reflect"
	"strings"
)

// Table object
type Table struct {
	Title        string
	rows         int
	avgrow       bool
	sumrow       bool
	columns      []*column
	header       *columnHeader // Headers tree structure for faster rendering
	dataType     reflect.Type  // Type of structs displayed in table
	fontmeter    *gofpdf.Fpdf  // Fpdf instance for measuring fonts
	fontSize     float64
	headerHeight float64
}

type columnHeader struct {
	title         string
	w             float64
	h             float64
	subHeaders    []*columnHeader
	col           *column
	childrenDepth int
}

type column struct {
	fieldIndex int // Index of field in structure associated with this column
	maxWidth   float64
	values     []string
}

// initTable create columns and headers from structure tags
func (t *Table) initTable(dataType reflect.Type) error {
	if dataType.Kind() != reflect.Struct {
		return errors.New("type is not a struct")
	}
	t.dataType = dataType // Lock table's struct type
	// rightmost edge column hierarchy
	hcolumn := []*columnHeader{new(columnHeader)}
	for i := 0; i < dataType.NumField(); i++ {
		colName := dataType.Field(i).Tag.Get("column")
		if colName == "" {
			continue
		}
		for level, subtitle := range strings.Split(colName, "|") {
			if len(hcolumn) > level+2 && hcolumn[level+1].title == subtitle {
				continue
			}
			colHeader := &columnHeader{title: subtitle}
			hcolumn[level].subHeaders = append(hcolumn[level].subHeaders, colHeader)
			hcolumn = append(hcolumn[:level+1], colHeader)
		}
		column := &column{fieldIndex: i}
		hcolumn[len(hcolumn)-1].col = column
		t.columns = append(t.columns, column)
	}
	t.header = hcolumn[0]
	return nil
}

func (t *Table) addrow(v reflect.Value) {
	for _, col := range t.columns {
		text := strEncoder(fmt.Sprint(v.Field(col.fieldIndex)))
		col.values = append(col.values, text)
		maxWidth := t.fontmeter.GetStringWidth(text)
		if maxWidth > col.maxWidth {
			col.maxWidth = maxWidth
		}
		// TODO: aggregate values
	}
	t.rows++
}

// AddRow add single row to table. Row should be struct with fields marked with tag `column`.
// Empty table accept any structure, but lock it type after successful addition.
// All consecutive AddRow and AddRows should be called with the same struct type.
func (t *Table) AddRow(row interface{}) error {
	val := reflect.ValueOf(row)
	if val.Kind() == reflect.Ptr {
		val = val.Elem()
	}
	if t.dataType == nil {
		if err := t.initTable(val.Type()); err != nil {
			return err
		}
	} else if t.dataType != val.Type() {
		return errors.New("inconsistent struct type") // Don't try to mix different types in single table
	}
	t.addrow(val)
	return nil
}

// AddRows add multiple rows to table. Rows should be array or slice of structs (see AddRow).
// After first successful addition all consecutive calls should provide same struct type.
func (t *Table) AddRows(rows interface{}) error {
	val := reflect.ValueOf(rows)
	if val.Kind() == reflect.Ptr {
		val = val.Elem()
	}
	if val.Kind() != reflect.Slice && val.Kind() != reflect.Array {
		return errors.New("rows is not array or slice of structs")
	}
	if t.dataType == nil {
		if err := t.initTable(val.Type().Elem()); err != nil {
			return err
		}
	} else if t.dataType != val.Type().Elem() {
		return errors.New("inconsistent struct type") // Don't try to mix different types in single table
	}

	for i := 0; i < val.Len(); i++ {
		t.addrow(val.Index(i))
	}
	return nil
}

func (t *Table) layout(pageWidth float64) {
	// first pass: set columns width and find max depth
	var traverse func(*columnHeader) (float64, int)
	traverse = func(header *columnHeader) (float64, int) {
		header.title = strEncoder(header.title)
		width := t.fontmeter.GetStringWidth(header.title) + 2
		if header.col != nil {
			width = math.Max(width, header.col.maxWidth+2)
		} else {
			childrenWidth := float64(0)
			for _, subheader := range header.subHeaders {
				cWidth, cDepth := traverse(subheader)
				childrenWidth += cWidth
				if cDepth > header.childrenDepth {
					header.childrenDepth = cDepth
				}
			}
			width = math.Max(width, childrenWidth)
		}
		header.w = width
		return width, header.childrenDepth + 1
	}
	fullWidth, maxDepth := traverse(t.header)
	scale := float64(1)
	ptSize, mmSize := t.fontmeter.GetFontSize()
	if fullWidth > pageWidth {
		scale = pageWidth / fullWidth
		t.fontSize = ptSize * scale
		t.header.w = pageWidth
	}
	hBase := mmSize + 2
	// second pas: scale widths and set height
	var traverse2 func(*columnHeader, float64, int)
	traverse2 = func(header *columnHeader, width float64, depth int) {
		if depth > 0 {
			header.h = hBase * float64(maxDepth-depth) / float64(header.childrenDepth+1)
		}
		header.w = width
		if header.col != nil {
			header.col.maxWidth = width
		} else {
			// calculate total children width (may be less then parent width)
			totalWidth := float64(0)
			for _, subheader := range header.subHeaders {
				totalWidth += subheader.w
			}
			// distribute available width between children according to their relative widths
			for _, subheader := range header.subHeaders {
				traverse2(subheader, width*subheader.w/totalWidth, depth+1)
			}
		}
	}
	traverse2(t.header, t.header.w, 0)
	t.headerHeight = hBase * float64(maxDepth-1)
}

func (t *Table) drawHeader(doc *gofpdf.Fpdf) {
	doc.SetFillColor(200, 200, 200)
	doc.SetFontSize(t.fontSize)
	var traverse func(*columnHeader)
	traverse = func(header *columnHeader) {
		vx := doc.GetX()
		doc.CellFormat(header.w, header.h, header.title, "1", 0, "C", true, 0, "")
		x, y := doc.GetXY()
		doc.SetXY(vx, y+header.h)
		for _, subheader := range header.subHeaders {
			traverse(subheader)
		}
		doc.SetXY(x, y)
	}
	traverse(t.header)
	doc.Ln(t.headerHeight)
}

// Draw generate pdf from table
func (t *Table) Draw(doc *gofpdf.Fpdf, state *drawState) {
	w, h := doc.GetPageSize()
	ml, _, mr, mb := doc.GetMargins()
	_, mmSize := doc.GetFontSize()
	pageWidth := w - ml - mr
	t.layout(pageWidth)
	doc.SetFontSize(14)
	doc.CellFormat(0, 15, strEncoder(t.Title), "", 1, "L", false, 0, "")

	// If header not fits on this page - start new one
	if doc.GetY()+t.headerHeight > h-mb {
		doc.AddPage()
	}
	t.drawHeader(doc)
	// Register pageStart func to display table header on fresh pages
	state.pageStart = func() {
		t.drawHeader(doc)
	}
	for i := 0; i < t.rows; i++ {
		for _, col := range t.columns {
			doc.CellFormat(col.maxWidth, mmSize+2, col.values[i], "1", 0, "C", false, 0, "")
		}
		doc.Ln(-1)
	}
	// TODO: draw footer (sum & avg)

	state.pageStart = nil
}
