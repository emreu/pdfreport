package pdfreport

import (
	"fmt"
	"io"

	"github.com/jung-kurt/gofpdf"
)

// ReportItem interface
type ReportItem interface {
	Draw(doc *gofpdf.Fpdf, state *drawState)
}

// Report structure
type Report struct {
	Title     string
	items     []ReportItem
	fontmeter *gofpdf.Fpdf
}

type drawState struct {
	pageStart func()
}

var strEncoder func(string) string

func init() {
	enc, err := gofpdf.UnicodeTranslatorFromFile("fonts/cp1251.map")
	if err != nil {
		fmt.Println(err)
		strEncoder = func(s string) string {
			return "?"
		}
	}
	strEncoder = enc
}

// New create new Report
func New(Title string) (r *Report) {
	r = &Report{Title: Title}

	r.fontmeter = gofpdf.New("P", "mm", "A4", "fonts")
	setFonts(r.fontmeter)
	return
}

// AddTable create empty table in report and return it for further operations
func (r *Report) AddTable(title string) *Table {
	table := &Table{Title: title}
	table.fontmeter = r.fontmeter
	table.fontSize = float64(12)
	r.items = append(r.items, table)
	return table
}

func (r *Report) generate() *gofpdf.Fpdf {
	pdf := gofpdf.New("P", "mm", "A4", "fonts")
	setFonts(pdf)
	state := &drawState{}
	pdf.AliasNbPages("")
	pdf.SetHeaderFunc(func() {
		// Draw common stuff
		pdf.SetFontSize(12)
		pdf.CellFormat(0, 10, strEncoder(r.Title), "b", 0, "L", false, 0, "")
		pdf.CellFormat(0, 10, fmt.Sprintf("%d / {nb}", pdf.PageNo()), "", 0, "R", false, 0, "")
		pdf.Ln(-1)
		pdf.Ln(4)
		// Draw custom page heading if set by
		if state.pageStart != nil {
			state.pageStart()
		}
	})
	pdf.AddPage()
	setFonts(pdf)
	for _, item := range r.items {
		item.Draw(pdf, state)
		pdf.Ln(10)
	}
	return pdf
}

// GenerateToWriter generate report content and stream it to writer
func (r *Report) GenerateToWriter(w io.Writer) {
	pdf := r.generate()
	pdf.Output(w)
}

// GenerateAndSave generate report content and save it to file
func (r *Report) GenerateAndSave(fname string) error {
	pdf := r.generate()
	return pdf.OutputFileAndClose(fname)
}
