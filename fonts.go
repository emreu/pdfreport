package pdfreport

import "github.com/jung-kurt/gofpdf"

// load fonts
func setFonts(pdf *gofpdf.Fpdf) {
	// TODO: Provide font loading from bindata
	pdf.AddFont("Verdana", "", "verdana.json")
	pdf.SetFont("Verdana", "", 12)
}
